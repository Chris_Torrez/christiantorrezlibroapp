﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Libro
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(cajaTitulo.Text) && !listaLibros.Items.Contains(cajaTitulo.Text))
                listaLibros.Items.Add(cajaTitulo.Text);

            if (!string.IsNullOrWhiteSpace(cajaNombre.Text) && !listaLibros.Items.Contains(cajaNombre.Text))
                listaLibros.Items.Add(cajaNombre.Text);

            if (!string.IsNullOrWhiteSpace(cajaAnio.Text) && !listaLibros.Items.Contains(cajaAnio.Text))
                listaLibros.Items.Add(cajaAnio.Text);

            if (!string.IsNullOrWhiteSpace(cajaEditorial.Text) && !listaLibros.Items.Contains(cajaEditorial.Text))
                listaLibros.Items.Add(cajaEditorial.Text);
        }

        private void MostrarButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(cajaTitulo.Text) && !listaLibros.Items.Contains(cajaTitulo.Text))
                listaLibros.Items.Add(cajaTitulo.Text);

            if (!string.IsNullOrWhiteSpace(cajaNombre.Text) && !listaLibros.Items.Contains(cajaNombre.Text))
                listaLibros.Items.Add(cajaNombre.Text);

            if (!string.IsNullOrWhiteSpace(cajaAnio.Text) && !listaLibros.Items.Contains(cajaAnio.Text))
                listaLibros.Items.Add(cajaAnio.Text);

            if (!string.IsNullOrWhiteSpace(cajaEditorial.Text) && !listaLibros.Items.Contains(cajaEditorial.Text))
                listaLibros.Items.Add(cajaEditorial.Text);
        }
    }
    
}
