﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Libro.Serialization;

namespace Libro.models
{
    class Libro
    {
        public string nombre { get; set; }
        public string nombreAutor { get; set; }
       // public DateTime ReleaseDate { get; set; }
        public string Editorial { get; set; }
        public string Categoria { get; set; }

        public Libro ReadObjectFromXml(string filePath)
        {
            return XmlSerialization.ReadFromXmlFile<Libro>(filePath);
        }

        public void WriteObjectInXml(string filePath)
        {
            XmlSerialization.WriteToXmlFile(filePath, this);
        }

    }
}
