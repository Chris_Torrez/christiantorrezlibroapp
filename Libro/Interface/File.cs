﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libro.Interface
{
    interface File<T>
    {
        T ReadObjectFromXml(string filePath);
        void WriteObjectInXml(string filePath);

    }
}
