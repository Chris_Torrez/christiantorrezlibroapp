﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Libro.Serialization
{
    class XmlSerialization
    {
        public static void WriteToXmlFile<T>(string filePath, T objectToWrite, bool append = false) where T : new()
        {
            using (TextWriter writer2 = new StreamWriter(filePath, append))
            {
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(writer2, objectToWrite);
            }
        }

        public static T ReadFromXmlFile<T>(string filePath) where T : new()
        {
            using (TextReader reader = new StreamReader(filePath))
            {
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(reader);
            }
        }

    }
}
